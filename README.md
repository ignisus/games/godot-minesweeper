# GodotMinesweeper
Minesweeper Godot Engine Flatpak [Play Online](https://ignisus.org/gaming)

Left click - Clear tile

Right click - Flag tile

Left, right, or middle click on a revealed tile will [chord](https://en.wikipedia.org/wiki/Chording#Minesweeper_tactic) it

![](https://i.imgur.com/0mmd5sZ.png)

![](https://i.imgur.com/v8kvCL4.png)
